-- librain helper class 
-- by Nicola Marangon

-- 1.0.2:
-- num_glasses auto assigned
-- new callbacks register_plugin_actived_callback & register_plugin_deactived_callback

-- 1.0.1:
-- a few debug output

__DEBUG__ = false

local librain = {}

function librain:cb_view_is_internal_stub()
	return true
end

function librain:cb_update_wipers_stub()
	return true
end

function librain:cb_plugin_actived_stub()
	return true
end

function librain:cb_plugin_deactived_stub()
	return true
end

function librain:new(obj)

	obj = obj or {}
	self.__index = self
	metatable = setmetatable(obj, self)

	-- librain drfs
	self.numglass = globalPropertyi("librain/num_glass_use")
	self.initialize = globalPropertyi("librain/initialize")
	self.init_success = globalPropertyi("librain/init_success")
	self.debug_draw = globalPropertyi("librain/debug_draw")
	self.wipers_visible = globalPropertyi("librain/wipers_visible")
	self.verbose = globalPropertyi("librain/verbose")

	-- default librain drf values
	self.set_default_max_tas = 100
	self.set_default_cabin_temp = 295
	self.set_default_therm_inertia = 25
	
	--
	self.ipad_is_installed = false
	self.plugin_is_installed = false
	self.plugin_is_ready = false
	self.is_active = false

	-- callbacks
	self.cb_view_is_internal = librain.cb_view_is_internal_stub
	self.cb_update_wipers = librain.cb_update_wipers_stub
	self.cb_set_cabin_temperature = nil
	self.cb_plugin_actived = librain.cb_plugin_actived_stub
	self.cb_plugin_deactived = librain.cb_plugin_deactived_stub

	-- glasses/z_depth_obj initialization
	self.glasses_idx = -1
	self.glasses = {}
	
	self.z_depth_obj_idx = -1
	self.z_depth_obj = {}

	self.wipers_idx = -1
	self.wipers = {}

	self:init_z_depth_obj_structure()

	-- x-plane drfs
	self.drf = {
		rain_percent = globalPropertyf("sim/weather/rain_percent"),
		pilots_head_x = globalPropertyf("sim/graphics/view/pilots_head_x"),
		pilots_head_y = globalPropertyf("sim/graphics/view/pilots_head_y"),
		pilots_head_z = globalPropertyf("sim/graphics/view/pilots_head_z"),
		view_is_external = globalPropertyi("sim/graphics/view/view_is_external")
	}

	return metatable

end

function librain:set_filename(glass, filename)
	set(glass.filename, filename)
end

function librain:set_gravity_xyf(glass, gp_x, gp_y, gravity_factor)

	set(glass.gp_x, gp_x)
	set(glass.gp_y, gp_y)
	set(glass.gravity_factor, gravity_factor)

end

function librain:set_wind_xyfn(glass, wp_x, wp_y, wind_factor, wind_normal)

	set(glass.wp_x, wp_x)
	set(glass.wp_y, wp_y)
	set(glass.wind_factor, wind_factor)
	set(glass.wind_normal, wind_normal)

end

function librain:set_thrust_xyfm(glass, tp_x, tp_y, thrust_factor, max_thrust)

	set(glass.tp_x, tp_x)
	set(glass.tp_y, tp_y)
	set(glass.thrust_factor, thrust_factor)
	set(glass.max_thrust, max_thrust)

end

function librain:set_slant_factor(glass, slant_factor)
	set(glass.slant_factor, slant_factor)
end

function librain:set_max_tas(glass, max_tas)
	set(glass.max_tas, max_tas)
end

function librain:set_cabin_temp(glass, cabin_temp)
	set(glass.cabin_temp, cabin_temp)
end

function librain:set_therm_inertia(glass, therm_inertia)
	set(glass.therm_inertia, therm_inertia)
end

function librain:set_wiper_px_py_ri_ro(wiper, pivot_x, pivot_y, radius_inner, radius_outer)

	set(wiper.pivot_x, pivot_x)
	set(wiper.pivot_y, pivot_y)
	set(wiper.radius_inner, radius_inner)
	set(wiper.radius_outer, radius_outer)

end

function librain:init_z_depth_obj_structure()

	for i = 0, 19 do

		self.z_depth_obj["obj" .. i .. "_filename"] = globalPropertys("librain/z_depth_obj_" .. i .. "/filename")
		self.z_depth_obj["obj" .. i .. "_load"] = globalPropertyi("librain/z_depth_obj_" .. i .. "/load")
		self.z_depth_obj["obj" .. i .. "_loaded"] = globalPropertyi("librain/z_depth_obj_" .. i .. "/loaded")

	end
	
end

function librain:glass_init_structure(glass, name, index)

	glass['name'] = name
	glass['index'] = index
	glass['filename'] = globalPropertys("librain/glass_" .. index .. "/obj/filename")
	glass['load'] = globalPropertyi("librain/glass_" .. index .. "/obj/load")
	glass['loaded'] = globalPropertyi("librain/glass_" .. index .. "/obj/loaded")
	glass['pos_offset_x'] = globalPropertyf("librain/glass_" .. index .. "/obj/pos_offset/x")
	glass['pos_offset_y'] = globalPropertyf("librain/glass_" .. index .. "/obj/pos_offset/y")
	glass['pos_offset_z'] = globalPropertyf("librain/glass_" .. index .. "/obj/pos_offset/z")
	glass['slant_factor'] = globalPropertyd("librain/glass_" .. index .. "/slant_factor")
	glass['gp_x'] = globalPropertyf("librain/glass_" .. index .. "/gravity_point/x")
	glass['gp_y'] = globalPropertyf("librain/glass_" .. index .. "/gravity_point/y")
	glass['gravity_factor'] = globalPropertyd("librain/glass_" .. index .. "/gravity_factor")
	glass['tp_x'] = globalPropertyf("librain/glass_" .. index .. "/thrust_point/x")
	glass['tp_y'] = globalPropertyf("librain/glass_" .. index .. "/thrust_point/y")
	glass['thrust_factor'] = globalPropertyd("librain/glass_" .. index .. "/thrust_factor")
	glass['max_thrust'] = globalPropertyd("librain/glass_" .. index .. "/max_thrust")
	glass['wp_x'] = globalPropertyf("librain/glass_" .. index .. "/wind_point/x")
	glass['wp_y'] = globalPropertyf("librain/glass_" .. index .. "/wind_point/y")
	glass['wind_factor'] = globalPropertyd("librain/glass_" .. index .. "/wind_factor")
	glass['wind_normal'] = globalPropertyd("librain/glass_" .. index .. "/wind_normal")
	glass['max_tas'] = globalPropertyd("librain/glass_" .. index .. "/max_tas")
	glass['cabin_temp'] = globalPropertyf("librain/glass_" .. index .. "/cabin_temp")
	glass['therm_inertia'] = globalPropertyf("librain/glass_" .. index .. "/therm_inertia")
	glass['heatzones'] = {}
	glass['temps'] = {}
	glass['hot_air_src'] = {}
	glass['hot_air_radius'] = {}
	glass['hot_air_temp'] = {}

	for i = 0, 16 do
		glass.heatzones[i] = globalPropertyf("librain/glass_" .. index .. "/heatzones[".. i .."]")	
	end
	
	for i = 0, 4 do

		glass.temps[i] = globalPropertyf("librain/glass_" .. index .. "/heat_tgt_temps[".. i .."]")
		glass.hot_air_src[i] = globalPropertyf("librain/glass_" .. index .. "/hot_air_src[".. i .."]")

	end

	for i = 0, 2 do
		glass.hot_air_radius[i] = globalPropertyf("librain/glass_" .. index .. "/hot_air_radius[".. i .."]")
		glass.hot_air_temp[i] = globalPropertyf("librain/glass_" .. index .. "/hot_air_temp[".. i .."]")
	end

end

function librain:add_glass(name)

	self.glasses_idx = self.glasses_idx + 1
	self.glasses[self.glasses_idx] = {}
	self:glass_init_structure(self.glasses[self.glasses_idx], name, self.glasses_idx)
	self:glass_reset_temps_and_heats(self.glasses[self.glasses_idx])
	set(self.numglass, self.glasses_idx + 1)
	
	return self.glasses[self.glasses_idx]
	
end

function librain:add_wiper(glass, wiper_index_sim, wiper_name)

	self.wipers_idx = self.wipers_idx + 1
	self.wipers[self.wipers_idx] = {}
	self:wiper_init_structure(self.wipers[self.wipers_idx], glass, self.wipers_idx, wiper_index_sim, wiper_name)
	
	return self.wipers[self.wipers_idx]
	
end

function librain:wiper_init_structure(wiper, glass, wiper_index, wiper_index_sim, wiper_name)

	wiper["name"] = wiper_name
	wiper["pivot_x"] = globalPropertyf("librain/glass_" .. glass['index'] .. "/wiper_" .. wiper_index .. "/pivot/x")
	wiper["pivot_y"] = globalPropertyf("librain/glass_" .. glass['index'] .. "/wiper_" .. wiper_index .. "/pivot/y")
	wiper["radius_inner"] = globalPropertyd("librain/glass_" .. glass['index'] .. "/wiper_" .. wiper_index .. "/radius_inner")
	wiper["radius_outer"] = globalPropertyd("librain/glass_" .. glass['index'] .. "/wiper_" .. wiper_index .. "/radius_outer")
	wiper["moving"] = globalPropertyf("librain/glass_" .. glass['index'] .. "/wiper_" .. wiper_index .. "/moving")
	wiper["angle"] = globalPropertyf("librain/glass_" .. glass['index'] .. "/wiper_" .. wiper_index .. "/angle")
	wiper["sim_wiper_angle_deg"] = globalPropertyf("sim/flightmodel2/misc/wiper_angle_deg[" .. wiper_index_sim .. "]")
	
end

function librain:add_z_depth_obj(filename)

	self.z_depth_obj_idx = self.z_depth_obj_idx + 1
	
	set(self.z_depth_obj["obj" .. self.z_depth_obj_idx .. "_filename"], filename)
	set(self.z_depth_obj["obj" .. self.z_depth_obj_idx .. "_load"], 1)
		
end

function librain:glass_reset_temps_and_heats(glass)

	for i = 0,16 do
		set(glass.heatzones[i], 0)
	end
	
	for i = 0,4 do
		set(glass.temps[i], 0)
		set(glass.hot_air_src[i], 0)
	end

	for i = 0, 2 do
		set(glass.hot_air_radius[i], 0)
		set(glass.hot_air_temp[i], 0)
	end

end

function librain:init_plugin(init_callback)

	if __DEBUG__ then logDebug('librain:init_plugin() init_success=' .. get(self.init_success)) end

	if get(self.init_success) == 0 then

		init_callback()

		local all_objects_loaded = true
		
		for i = 0, self.glasses_idx do
			
			if get(self.glasses[i].loaded) == 0 then
				set(self.glasses[i].load, 1)
			end
			
			all_objects_loaded = all_objects_loaded and get(self.glasses[i].loaded) == 1		
		
		end
		
		for i = 0, self.z_depth_obj_idx do
			all_objects_loaded = all_objects_loaded and get(self.z_depth_obj["obj" .. i .. "_loaded"]) == 1
		end

		self.plugin_is_ready = all_objects_loaded

		if __DEBUG__ then logDebug('librain:init_plugin() plugin_is_ready=' .. tostring(self.plugin_is_ready)) end

	end

end

function librain:unload()

	if __DEBUG__ then logDebug('librain:unload() initialize=' .. get(self.initialize)) end
	
	if get(self.initialize) ~= nil then

		set(self.initialize, 0)

		for i = 0, self.glasses_idx do		
			set(self.glasses[i].load, 0)
		end
		
		for i = 0, self.z_depth_obj_idx do
			set(self.z_depth_obj["obj" .. i .. "_load"], 0)
		end
		
		self.plugin_is_ready = false

	end

end

function librain:register_view_is_internal_callback(view_is_internal_callback)
	self.cb_view_is_internal = view_is_internal_callback
end

function librain:register_update_wipers_callback(update_wipers_callback)
	self.cb_update_wipers = update_wipers_callback
end

function librain:register_update_cabin_temperature_callback(cabin_temperature_callback)
	self.cb_set_cabin_temperature = cabin_temperature_callback
end

function librain:register_plugin_actived_callback(plugin_actived_callback)
	self.cb_plugin_actived = plugin_actived_callback
end

function librain:register_plugin_deactived_callback(plugin_deactived_callback)
	self.cb_plugin_deactived = plugin_deactived_callback
end

function librain:update()
	
	if self.plugin_is_ready then
		
		local show_rain = get(self.initialize) ~= nil and get(self.drf.rain_percent) ~= 0 and self.cb_view_is_internal()			
		if show_rain then
			
			if not self.is_active then 
				if __DEBUG__ then logDebug('librain:update(); show_rain=true') end
				set(self.initialize, 1) 
				self.is_active = true
				self.cb_plugin_actived()
			end
			
			self.cb_update_wipers()

		else

			if __DEBUG__ then
				logDebug('librain:update() show_rain=false; initialize=' .. get(self.initialize) .. '; rain_percent=' .. get(self.drf.rain_percent) .. '; view_is_internal=' .. tostring(self.cb_view_is_internal())) 
			end

			if self.is_active then 
				set(self.initialize, 0) 
				self.is_active = false
				self.cb_plugin_deactived()
			end

		end
		
	end

end

return librain
