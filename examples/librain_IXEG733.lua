-- librain integration for IXEG 737 Classic
-- v1.0
-- by Nicola Marangon

package.path = panelDir.."/Custom Avionics/?.lua"

local librain_helper = require 'librain_helper'

local acf_folder_mod = getAircraftPath()

local librain = librain_helper:new()

local ixeg_show_windscreen = globalPropertyi("ixeg/733/prefs/show_windscreen")
local ixeg_show_avitab = globalPropertyi("ixeg/733/misc/show_avitab")

function init_plugin_callback()

	set(librain.verbose, 1)
	set(librain.debug_draw, 0)
	set(librain.wipers_visible, 0)

	-------------------------------------------------------------------
	front_glass = librain:add_glass("front_glass")
	left_glass = librain:add_glass("left_glass")
	right_glass = librain:add_glass("right_glass")

	-------------------------------------------------------------------
	librain:set_filename(front_glass, acf_folder_mod.."objects/rain/rain_front.obj")	
	librain:set_slant_factor(front_glass, 1)
	librain:set_gravity_xyf(front_glass, 0.5, 40, 0.5)
	librain:set_wind_xyfn(front_glass, 0.5, -2, 1, 1)
	librain:set_thrust_xyfm(front_glass, 0.5, -2, 0, 0)

	librain:set_filename(left_glass, acf_folder_mod.."objects/rain/rain_left.obj")	
	librain:set_slant_factor(left_glass, 1)
	librain:set_gravity_xyf(left_glass, 0.5, 20, 0.5)
	librain:set_wind_xyfn(left_glass, 1, 0.5, 1, 1)
	librain:set_thrust_xyfm(left_glass, 1, 0.5, 0, 0)

	librain:set_filename(right_glass, acf_folder_mod.."objects/rain/rain_right.obj")	
	librain:set_slant_factor(right_glass, 1)
	librain:set_gravity_xyf(right_glass, 0.5, 20, 0.5)
	librain:set_wind_xyfn(right_glass, 0, 0.5, 1, 1)
	librain:set_thrust_xyfm(right_glass, 0, 0.5, 0, 0)

	librain:set_max_tas(front_glass, 80)	

	wiper_left = librain:add_wiper(front_glass, 0, 'left')
	wiper_right = librain:add_wiper(front_glass, 1, 'right')

	-- IXEG custom wipers drefs 	
	wiper_left['wiper_speed'] = globalPropertyi("ixeg/733/wiper/wiper_speed_act")
	wiper_left['wiper_angle_deg'] = globalPropertyf("ixeg/733/wiper/wiper_angle_ratio")
	
	librain:set_wiper_px_py_ri_ro(wiper_left, 0.47, 0, 0.1, 0.54)
	librain:set_wiper_px_py_ri_ro(wiper_right, 0.64, 0, 0.1, 0.52)

	-------------------------------------------------------------------
	--librain:add_z_depth_obj(acf_folder_mod.."B733_cockpit_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_cockpit_shell_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_cockpit_ECAM_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_cockpit_STEAM_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_overhead_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_walls_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_glareshield_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_thro_quad_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_misc_ECAM_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_misc_STEAM_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_pedestal_281.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/B733_seats_281.obj")
    	
	
	if isFileExists(acf_folder_mod.."objects/ipad.obj1") then
		librain:add_z_depth_obj(acf_folder_mod.."objects/ipad.obj")
		librain.ipad_is_installed = true
	end

end

function onAvionicsDone()
	librain:unload()
end

function view_is_internal_callback()
	return get(librain.drf.view_is_external) == 0 
end

function update_wipers_callback()

	if get(wiper_left.wiper_speed) > 0 then
	
		set(wiper_left.angle, math.rad((get(wiper_left.wiper_angle_deg) * 90 - 90)) * 0.6)
		set(wiper_left.moving, 1)			

		set(wiper_right.angle, math.rad((1 - get(wiper_left.wiper_angle_deg)) * 90 - 5) * 0.52)
		set(wiper_right.moving, 1)

	else

		set(wiper_left.angle, 0)	
		set(wiper_left.moving, 1)							

		set(wiper_right.angle, 0)	
		set(wiper_right.moving, 1)							

	end	
	
end

function plugin_activated_callback()
	set(ixeg_show_windscreen, 0)
end

function plugin_deactiveted_callback()
	set(ixeg_show_windscreen, 1)
end

logDebug('librain:start')

librain:register_view_is_internal_callback(view_is_internal_callback)
librain:register_update_wipers_callback(update_wipers_callback)
librain:register_plugin_actived_callback(plugin_activated_callback)
librain:register_plugin_deactived_callback(plugin_deactiveted_callback)

librain:init_plugin(init_plugin_callback)

function update()
	librain:update()
end
