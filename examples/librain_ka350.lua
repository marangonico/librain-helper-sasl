-- librain integration for AFL KA350
-- v1.0
-- by Nicola Marangon

package.path = panelDir.."/Custom Avionics/?.lua"

local librain_helper = require 'librain_helper'

local acf_folder_mod = getAircraftPath()

local librain = librain_helper:new()

function init_plugin_callback()

	set(librain.verbose, 1)
	set(librain.debug_draw, 0)
	set(librain.wipers_visible, 0)

	-------------------------------------------------------------------
	front_glass = librain:add_glass("front_glass")
	left_glass = librain:add_glass("left_glass")
	right_glass = librain:add_glass("right_glass")

	-------------------------------------------------------------------
	librain:set_filename(front_glass, acf_folder_mod.."objects/rain/rain_front.obj")	
	librain:set_slant_factor(front_glass, 1)
	librain:set_gravity_xyf(front_glass, 0.5, 40, 0.5)
	librain:set_wind_xyfn(front_glass, 0.5, -2, 1, 1)
	librain:set_thrust_xyfm(front_glass, 0.5, -2, 0, 0)

	librain:set_filename(left_glass, acf_folder_mod.."objects/rain/rain_left.obj")	
	librain:set_slant_factor(left_glass, 0.5)
	librain:set_gravity_xyf(left_glass, 0.5, 20, 0.5)
	librain:set_wind_xyfn(left_glass, 1, 0.5, 1, 1)
	librain:set_thrust_xyfm(left_glass, 1, 0.5, 0, 0)
	
	librain:set_filename(right_glass, acf_folder_mod.."objects/rain/rain_right.obj")	
	librain:set_slant_factor(right_glass, 0.5)
	librain:set_gravity_xyf(right_glass, 0.5, 20, 0.5)
	librain:set_wind_xyfn(right_glass, 0, 0.5, 1, 1)
	librain:set_thrust_xyfm(right_glass, 0, 0.5, 0, 0)

	librain:set_max_tas(front_glass, 80)	

	wiper_left = librain:add_wiper(front_glass, 0, 'left')
	wiper_right = librain:add_wiper(front_glass, 1, 'right')
	--
	---- KA350 custom wipers drefs 	
	wiper_left['wiper_speed'] = globalPropertyi("KA350/ianim/ohPanel/wiper")
	wiper_left['wiper_angle_deg'] = globalPropertyf("KA350/ianim/lWiper/pos")
	
	wiper_right['wiper_angle_deg'] = globalPropertyf("KA350/ianim/rWiper/pos")

	librain:set_wiper_px_py_ri_ro(wiper_left, 0.4, 0, 0.145, 0.345)
	librain:set_wiper_px_py_ri_ro(wiper_right, 0.6, 0, 0.145, 0.345)
	--
	---------------------------------------------------------------------
	librain:add_z_depth_obj(acf_folder_mod.."objects/h_int_cockpit.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/h_cockpit_1.obj")
  
	--if isFileExists(acf_folder_mod.."objects/ipad.obj") then
	--	librain:add_z_depth_obj(acf_folder_mod.."objects/ipad.obj")
	--	librain.ipad_is_installed = true
	--end

end

function onAvionicsDone()
	librain:unload()
end

function view_is_internal_callback()
	
	return get(librain.drf.view_is_external) == 0 
		and get(librain.drf.pilots_head_x) > -0.7 and get(librain.drf.pilots_head_x) < 0.7
		and get(librain.drf.pilots_head_y) > -0.5 and get(librain.drf.pilots_head_y) < 0.92
		and get(librain.drf.pilots_head_z) > -2.24 and get(librain.drf.pilots_head_z) < -1.21
		
end

function update_wipers_callback()

	if get(wiper_left.wiper_speed) > 0 then
	
	
		set(wiper_left.angle, math.rad((get(wiper_left.wiper_angle_deg) * -50 )) )
		set(wiper_left.moving, 1)			
	
		set(wiper_right.angle, math.rad((1 - get(wiper_right.wiper_angle_deg)) * -50 + 50))
		set(wiper_right.moving, 1)
	
	else
	
		set(wiper_left.angle, 0)	
		set(wiper_left.moving, 1)							
	
		set(wiper_right.angle, 0)	
		set(wiper_right.moving, 1)							
	
	end	
	
end


librain:register_view_is_internal_callback(view_is_internal_callback)
librain:register_update_wipers_callback(update_wipers_callback)

librain:init_plugin(init_plugin_callback)

function update()
	librain:update()
end
