package.path = panelDir.."/Custom Avionics/?.lua"

local librain_helper = require 'librain_helper'

local __FRONT_SLANT_FACTOR__ = 1.7

local acf_folder_mod = getAircraftPath()

local librain = librain_helper:new()

function init_plugin_callback()

	set(librain.verbose, 1)
	set(librain.debug_draw, 0)
	set(librain.wipers_visible, 0)
	set(librain.numglass, 3)

	-------------------------------------------------------------------
	front_glass = librain:add_glass("front_glass")
	left_glass = librain:add_glass("left_glass")
	right_glass = librain:add_glass("right_glass")

	-------------------------------------------------------------------
	librain:set_filename(front_glass, acf_folder_mod.."objects/librain/rain_front.obj")
	librain:set_slant_factor(front_glass, __FRONT_SLANT_FACTOR__)
	librain:set_gravity_xyf(front_glass, 0.5, 2, 0.5)
	librain:set_wind_xyfn(front_glass, 0.5, 0, 1, 1)
	librain:set_thrust_xyfm(front_glass, 0.5, 0, 0, 0)
	
	wiper_left = librain:add_wiper(front_glass, 0, 'left')
	wiper_right = librain:add_wiper(front_glass, 1, 'right')

	-- EMB110 v3 custom wipers drefs 
	wiper_left['wiper_switch'] = globalPropertyi("EMB110/switches/wiper[0]")
	wiper_left['wiper_speed'] = globalPropertyi("EMB110/switches/wiper_speed[0]")
	wiper_left['wiper_angle_deg'] = globalPropertyf("EMB110/misc/wiper_angle_deg[0]")

	wiper_right['wiper_switch'] = globalPropertyi("EMB110/switches/wiper[1]")
	wiper_right['wiper_speed'] = globalPropertyi("EMB110/switches/wiper_speed[1]")
	wiper_right['wiper_angle_deg'] = globalPropertyf("EMB110/misc/wiper_angle_deg[1]")

	librain:set_wiper_px_py_ri_ro(wiper_left, 0.25, 1.3, 0.54, 0.83)
	librain:set_wiper_px_py_ri_ro(wiper_right, 0.73, 1.3, 0.54, 0.83)

	-------------------------------------------------------------------
	librain:set_filename(left_glass, acf_folder_mod.."objects/librain/rain_left.obj")
	librain:set_slant_factor(left_glass, __FRONT_SLANT_FACTOR__ * 0.55)
	librain:set_gravity_xyf(left_glass, 0.5, 2, 0.8)
	librain:set_wind_xyfn(left_glass, 1.5, 0, 1, 1)
	librain:set_thrust_xyfm(left_glass, 1.5, 0, 0, 0)

	-------------------------------------------------------------------
	librain:set_filename(right_glass, acf_folder_mod.."objects/librain/rain_right.obj")
	librain:set_slant_factor(right_glass, __FRONT_SLANT_FACTOR__ * 0.55)
	librain:set_gravity_xyf(right_glass, 0.5, 2, 0.8)
	librain:set_wind_xyfn(right_glass, -1.5, 0, 1, 1)
	librain:set_thrust_xyfm(right_glass, -1.5, 0, 0, 0)

	-------------------------------------------------------------------
	librain:add_z_depth_obj(acf_folder_mod.."objects/painel.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/seat_overhead.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/manetes.obj")
	librain:add_z_depth_obj(acf_folder_mod.."objects/cabin_wall.obj")
		
	if isFileExists(acf_folder_mod.."objects/ipad.obj") then
		librain:add_z_depth_obj(acf_folder_mod.."objects/ipad.obj")
		librain.ipad_is_installed = true
	end

end

function onAvionicsDone()
	librain:unload()
end

function view_is_internal_callback()

	return get(librain.drf.view_is_external) == 0 
		and get(librain.drf.pilots_head_x) > -0.88 and get(librain.drf.pilots_head_x) < 0.82 
		and get(librain.drf.pilots_head_y) > 0.3 and get(librain.drf.pilots_head_y) < 1.75 
		and get(librain.drf.pilots_head_z) > -4.33 and get(librain.drf.pilots_head_z) < 5.65

end

function update_wipers_callback()

	if get(wiper_left.wiper_switch) > 0 then
		set(wiper_left.angle, math.max(-3.141592, math.rad(get(wiper_left.wiper_angle_deg, 1) / 6.5 - 180)))
		set(wiper_left.moving, 1)							
	else
		set(wiper_left.angle, 0)	
		set(wiper_left.moving, 1)							
	end	
	
	if get(wiper_right.wiper_switch) > 0 then
		set(wiper_right.angle, math.min(3.141592, math.rad(-get(wiper_right.wiper_angle_deg, 1) / 6.5 + 180)))
		set(wiper_right.moving, 1)
	else
		set(wiper_right.angle, 0)	
		set(wiper_right.moving, 1)							
	end	
	
end

librain:register_view_is_internal_callback(view_is_internal_callback)
librain:register_update_wipers_callback(update_wipers_callback)
librain:init_plugin(init_plugin_callback)

function update()
	librain:update()
end