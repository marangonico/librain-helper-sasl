# Librain helper for sasl #

This module is meant to provide help to those volenterous guys, who want to create integration mods for the great 
[librain plugin](https://github.com/skiselkov/librain) by Saso Kiselkov  

### compatibility ###
This module has been used to develop various mods with sasl v2. 

### usage ###
Study the examples provided here and their distribution versions downloadable from x-plane.org:

* [Librain integration for Dreamfoil Embraer 110 v3](https://forums.x-plane.org/index.php?/files/file/56004-librain-integration-for-dreamfoil-embraer-110-v3/)

* [Librain integration for AirFoilLabs King Air 350](https://forums.x-plane.org/index.php?/files/file/56464-librain-integration-for-airfoillabs-king-air-350/)

* [Librain integration for IXEG 737 Classic](https://forums.x-plane.org/index.php?/files/file/56406-librain-integration-for-ixeg-737-classic/)

### history ###
##### 1.0.2: #####
* num_glasses auto assigned
* new callbacks register_plugin_actived_callback & register_plugin_deactived_callback

##### 1.0.1: #####
* few debug output
